---
#Title: 'Miniaturisiertes, energieoptimiertes Ganganalysesystem zur \nobreak{Ermittlung} der Beweglichkeit bei Morbus Parkinson'
Title: 'Miniaturised, energy-optimised gait analysis system for determining mobility in Parkinson''s disease'
FirstAuthor:
   Title: false
   Forename: 'Wolfgang'
   Surename: 'Kilian'
   OrganizationShort: 'a'
OtherAuthor:
   -
	   Title: 'Dr.'
	   Forename: 'Stephan'
	   Surename: 'Odenwald'
	   OrganizationShort: 'a'
Organization:
   -
       Shortcut: 'a'
       CompanyName: 'Technische Universität Chemnitz'
       Place: 'Chemnitz'
       Country: 'Germany'
Thanks: 'Identify applicable funding agency here. If none, delete this.'
Abstract: 'Abstract'
# numbersections: true #dont use it is defined in the vde template
bibliography: '../example/literature/literature.bib'
watermark: 'Draft'
#polyglossia: false
defaultlanguage: english
defaultlanguageoption: variant=british
otherlanguage: german
otherlanguageoptions: spelling=new
csl-refs: true
csl-hanging-indent: false
csl: './vde.csl'
glossary: '../example/myglossary.tex'
printglossary: false
listings: true
multicols: false # my won't work well with two column design
codeBlockCaptions: true
balance: true
flushend: false
figureTitle: 'figure:'
tabelTitle: 'table:'
figPrefix: 'Figure'
#figureTemplate: '*$$figureTitle$$ $$i$$*$$titleDelim$$ $$t$$'
tblPrefix: 'Table'
# ieee compatibility switches
siunitx: true
floathelper: true
rotatesupport: false
unicodemath: true
hyperref: true
polyglossia: true
---