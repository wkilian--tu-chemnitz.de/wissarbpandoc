---
pandocomatic:
   use-template: Tspringer
---

!include ./header_springer.md

!include ./springer_abstract.md

\setkeys{Gin}{width=0.80\textwidth}



!include ../example/codestyle.md

!include ./springer_specific.md

!include ./paper_example_all.md


# References {-}
\footnotesize



