---
pandocomatic:
   use-template: Tvde
---

!include ./header_vde.md

# Citing
!include ../example/citing.md

\section{Literature}