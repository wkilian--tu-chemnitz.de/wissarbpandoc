---
Title: 'Contribution Title'
# If the paper title is too long for the running head, you can set an abbreviated paper title here
Titlerunning: 'Short Title'
Thanks: 'Supported by organization x.'
FirstAuthor:
   Name: 'First Author'
   Institution: '1'
   OrcidID: '0000-1111-2222-3333'
OtherAuthor:
   -
      Nr: 2
      Name: 'Second Author'
      Institution: '2,3'
      OrcidID: '1111-2222-3333-4444'
   -
      Nr: 3
      Name: 'Third Author'
      Institution: '3'
      OrcidID: '2222-3333-4444-5555'
Authorrunning: 'F. Author et al.'
# First names are abbreviated in the running head.
# If there are more than two authors, 'et al.' is used.
Institute:
   Name: 'Princeton University, Princeton NJ 08544, USA'
   Email: 'Mail@schnuff.de'
OtherInstitute:
   -
      Name: 'Springer Heidelberg, Tiergartenstr. 17, 69121 Heidelberg, Germany'
      Email: 'lncs@springer.com'
      Url: 'http://www.springer.com/gp/computer-science/lncs'
   -
      Name: 'ABC Institute, Rupert-Karls-University Heidelberg, Heidelberg, Germany'
      Email: '{abc,lncs}\@uni-heidelberg.de'
Keywords:
  - 'First keyword'
  - 'Second keyword'
  - 'Another keyword'
numbersections: true
bibliography: '../example/literature/literature.bib'
watermark: 'DRAFT'
#polyglossia: false
defaultlanguage: english
defaultlanguageoption: variant=british
otherlanguage: german
otherlanguageoptions: spelling=new
csl-refs: yes
csl-hanging-indent: yes
csl: 'springer-lecture-notes-in-computer-science.csl' # as mentioned in the original springer template
glossary: '../example/myglossary.tex'
printglossary: false
listings: true
multicols: true
codeBlockCaptions: true
balance: false
# ieee compatibility switches
siunitx: true
floathelper: true
rotatesupport: false
unicodemath: false
hyperref: true
polyglossia: false
ieeestealthmode: false
pdflatexmode: false
---

