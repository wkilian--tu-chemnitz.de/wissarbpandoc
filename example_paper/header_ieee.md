---
Title: 'Inside the useless box - (how to) stress between realitys'
FirstAuthor:
   Name: 'Wolfgang Kilian'
   Organization: 'dept. name of organization (of Aff.)'
   NameOrganization: 'name of organization (of Aff.)'
   City: 'City'
   Country: 'Country'
   Email: 'email address or ORCID'
OtherAuthor:
   -
      Nr: 2
      Name: 'Max Mustermann'
      Organization: 'Dept. name of organization (of Aff.)'
      NameOrganization: 'Name of organization (of Aff.)'
      City: 'City'
      Country: 'Country'
      Email: 'Email address or ORCID'
   -
      Nr: 3
      Name: 'The Boss'
      Organization: 'Dept. name of organization (of Aff.)'
      NameOrganization: 'Name of organization (of Aff.)'
      City: 'City'
      Country: 'Country'
      Email: 'Email address or ORCID'
Thanks: 'Identify applicable funding agency here. If none, delete this.'
Abstract: 'Abstract'
numbersections: true
bibliography: '../example/literature/literature.bib'
watermark: 'DRAFT'
#polyglossia: false
defaultlanguage: english
defaultlanguageoption: variant=british
otherlanguage: german
otherlanguageoptions: spelling=new
csl-refs: yes
csl-hanging-indent: yes
csl: 'IEEEtran.csl'
glossary: '../example/myglossary.tex'
printglossary: false
listings: true
multicols: true
codeBlockCaptions: true
balance: false
# ieee compatibility switches
siunitx: true
floathelper: true
rotatesupport: false
unicodemath: false
hyperref: true
polyglossia: false
ieeestealthmode: false
pdflatexmode: false
---

