---
Title: 'Radar based gait analysis for patients with multiple sclerosis'
# If the paper title is too long for the running head, you can set an abbreviated paper title here
Titlerunning: 'Radar based gait analysis for MS'
Thanks: 'Hybrid Society (DFG)'
FirstAuthor:
  Name: 'Stefanie Doetz'
  OrcidID: '0000-1111-2222-3333'
  Institute:
    Name: 'Technische Universität Chemnitz, Germany'
    Email: 'stdo@hrz.tu-chemnitz.de'
    Url: 'https://www.tu-chemnitz.de'
OtherAuthor:
   -
      Nr: 2
      Name: 'Wolfgang Kilian'
      OrcidID: '1111-2222-3333-4444'
      Institute:
        Name: 'Technische Universität Chemnitz, Germany'
        Email: 'wkilian@hrz.tu-chemnitz.de'
        Url: 'https://www.tu-chemnitz.de'
   -
      Nr: 3
      Name: 'Aline Püschel'
      OrcidID: '1111-2222-3333-4444'
      Institute:
        Name: 'Technische Universität Chemnitz, Germany'
        Email: 'alpu@hrz.tu-chemnitz.de'
        Url: 'https://www.tu-chemnitz.de'
   -
      Nr: 4
      Name: 'Stephan Odenwald'
      OrcidID: '1111-2222-3333-4444'
      Institute:
        Name: 'Technische Universität Chemnitz, Germany'
        Email: 'odenwald@hrz.tu-chemnitz.de'
        Url: 'https://www.tu-chemnitz.de'
Authorrunning: 'S. Doetz et al.'
# First names are abbreviated in the running head.
# If there are more than two authors, 'et al.' is used.
Keywords:
  - 'human gait'
  - 'radar'
  - 'multiple sclerosis'
numbersections: true
bibliography: '../example/literature/literature.bib'
watermark: 'DRAFT'
defaultlanguage: english
defaultlanguageoption: variant=british
otherlanguage: german
otherlanguageoptions: spelling=new
csl-refs: yes
csl-hanging-indent: yes
csl: './CSL/apalike.csl' 
glossary: '../example/myglossary.tex'
printglossary: false
listings: true
multicols: true
codeBlockCaptions: true
balance: false
siunitx: true
floathelper: false
rotatesupport: true
unicodemath: true
hyperref: true
# the svmult document class dont like hyperref
fixHyperrefSvmult: true
polyglossia: true
ieeestealthmode: false
pdflatexmode: false
---
