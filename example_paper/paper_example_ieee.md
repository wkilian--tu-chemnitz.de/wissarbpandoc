---
pandocomatic:
   use-template: Tieee
---

!include ./header_ieee.md

!include ../example/codestyle.md

!include ./ieee_abstract.md

<!---
with the \setkey latex command we can set keys that are used eg to change every graphic.
With the following statement it is possible to scale every following figure (if not defined there). This is very helpful if (like in this case) we have a two column paper
-->
\setkeys{Gin}{width=0.45\textwidth}

!include ./paper_example_all.md

# References {-}
\footnotesize



