Sections are easy to use, it is plain markdown. Some more hints how you can use it more "special" given here:

```
# Main section

## Sub section

### Sub Sub Section
```

and keep in mind to *always* put a new line after the headline.

if you do not want to count a specific section use it like this:

```
## not counted sub section {-}
```
## not counted sub section {-}
```
and if you want to have a reference to a section 
```

```
## Subsection with reference {#sec:RefSec}
```

will get to this:

## Subsection with reference {#sec:RefSec}


```
and you cen refer as usual in markdown - as shown in @sec:RefSec
```

and you can refer as usual in markdown - as shown in @sec:RefSec
