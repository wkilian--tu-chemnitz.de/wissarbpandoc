We can make use of abreviations and acronyms and the system takes care to introduce it for you and we can even create a glossary if needed.

The pandoc filter that is used is the "pandoc-gls.lua" - have a look at the docker source if you need more informations.

 https://github.com/tomncooper/pandoc-gls.git

Just use it like this:

```
(+BAR) or (+FOO)
```
If you want to use the abreviation "BAR" and it will be translated into --> (+BAR) and if you use it again like this (+BAR) again, it will be translated like expected.

The use for the glossary is exactly the same. So we want to introduce "FOO" and just write it - it will be translated to (+FOO) and if we use it again we will have (+FOO) again...

The definitions are inside the "myglossary.tex" file so that they can stay out of your focus.
```latex
!include myglossary.tex
```
