---
style: 'SciWork' % This style definition is used by panzer
title: 'Das hat noch gar keinen Namen' % the title for the thesis itself
subtitle: '(optional) Hier ist die nähere Erläuterung' % if some subtitle is needed
author:  Wolfgang Kilian % someone is responsible for the thesis
date: 27-05-2019 % this is writen on the pages
## polyglossia is used --> default language, others and options, hope selfexplaining
defaultlanguage: german
defaultlanguageoptions: spelling=new
otherlanguage: english
otherlanguageoption: variant=british
bibliography: Literatur/Literatur.bib % where is the bibliography file located?
### to be included
#reference-section-title: Literaturverzeichnis
#toc-own-page: true
#numbersections: true
#titlepage: true
#caption-justification: centering
#fontsize: 11pt
#figureTitle: "Abbildung"
#tableTitle: "Tabelle"
#secTitle: "Absatz"
#figPrefix: "Abb."
#eqnPrefix: "Gl."
#tblPrefix: "Tbl."
#secPrefix: "Abs."
#loftitle: "# Abbildungsverzeichnis"
#lotTitle: "# Tabellenverzeichnis"
#titlepage: true
#toc: true
#toc-own-page: true
#numbersections: true
#mainfont: Font-Regular.otf
#mainfontoptions:
#  - BoldFont=Font-Bold.otf
#  - ItalicFont=Font-Italic.otf
#  - BoldItalicFont=Font-BoldItalic.otf
#fontfamily: merriweather
#fontfamilyoptions: sfdefault
#mainfont: 'Palatino'
# Sektionen - ob das so klappt?
# include wird anders gemacht, zumindest laut ``pandoc-include''
#abstract: 'include/abstract'
#introduction: 'include/introduction'
#chapters:
#    - 'include/bla1'
#    - 'include/blubber'
#    - 'include/next'
#    - 'include/overview'
#conclusion: 'include/conclusion'
#Watermark: 'Entwurf' # Wenn das gesetzt ist, dann kommt wird es erzeugt, wenn nicht, ignoriert
#mainfont: XITS
## Das muss auch alles noch in die eigentliche Vorlage, dass Template integriert werden
#header-includes:
#      - \usepackage{unicode-math}
#      - \setmathfont{XITS Math}
#      - \setmathfont[range={\mathcal,\mathbfcal},StylisticSet=1]{XITS Math}
#      - \usepackage{multicol}
#      - \usepackage{pstricks}
#      - \usepackage{pst-barcode}
#      - \usepackage{draftwatermark}
#      - \SetWatermarkText{Entwurf}
#      - \SetWatermarkLightness{0.95}
#      - \newcommand{\hideFromPandoc}[1]{#1}\hideFromPandoc{\let\Begin\begin\let\End\end}
#commandline:
#  csl: "`citestyle.csl`"
---

Hauptdokument




