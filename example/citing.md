The importand citing thing. You can make it easy:

This:
```
We want to reference to [@Brooks1986] and the plugin will do the rest.
``` 

will become to this:

We want to reference to [@Brooks1986] and the plugin will do the rest.

If we have more than one reference we can do this as well:

```
[@Brooks1986; @thrun2005; @hol2006]
```

Some robotsience can bee seen here [@Brooks1986; @thrun2005; @hol2006].

```
If we refere to some specific page [@Brooks1986, pp. 10]
``` 

If we refer to some specific page [@Brooks1986, pp. 10]

More complex:

```
First version [@hol2006{ii, A, D-Z}, with a suffix].  
Second version [@hol2006, {pp. iv, vi-xi, (xv)-(xvii)} with suffix here].  
Third version [@hol2006{}, 99 years later].  
```

First version [@hol2006{ii, A, D-Z}, with a suffix].  
Second version [@hol2006, {pp. iv, vi-xi, (xv)-(xvii)} with suffix here].  
Third version [@hol2006{}, 99 years later].  


A minus sign (-) before the "@" will suppress mention of the author in the citation. This can be useful when the author is already mentioned in the text:

```
Smith says blah [-@hol2006].
```

Smith says blah [-@hol2006].  

More informations can be found here:  

https://pandoc.org/MANUAL.html#extension-citations
  
**Keep in mind - HOW the citing is done is specified in the header combinde with a propper CSL file!**




