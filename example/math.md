Math is done basically the same way you would write it in mathtex. So this:

(be careful, the syntax changed from '\\SI' to a much more descriptive system)

```latex
Some math: $a_{2}^{5}x \text{ bla } \dot{\vec{y}}$ (inline) and as we use the siunix we can write
numbers $\num{123,4}$
or numrange $\numrange{1}{10}$
angle \ang{12.3} or other angle notations \ang{1;2;3}

Units:

$\unit{\newton} = \unit{\kilo\gram\metre\per\square\second}$  

Numbers with units (aka quantity)

$\qty{.23e7}{\candela}$

and a little more complex:

$\qty[per-mode = fraction]{1,345}{\coulomb\per\mole}$


there are more commonly used variants of quantity combinations:

$\qtylist{10;30;45}{\metre}$
$\qtyproduct{10 x 30 x 45}{\metre}$

Ranges works as well:

$\qtyrange{10}{30}{\metre}$
$\numrange{1}{10}$

AND if we need to reference some equations:

$$E = m \cdot c^{2}$$ {#eq:Einstein}

(works only with \$\$ ...)
and this gives us informations about the warp here @eq:Einstein and more whereever.
```

is rendered to this:  

Some math: $a_{2}^{5}x \text{ bla } \dot{\vec{y}}$ (inline) and as we use the siunix we can write
numbers $\num{123,4}$
or numrange $\numrange{1}{10}$
angle \ang{12.3} or other angle notations \ang{1;2;3}

Units:

$\unit{\newton} = \unit{\kilo\gram\metre\per\square\second}$  

Numbers with units (aka quantity)

$\qty{.23e7}{\candela}$

and a little more complex:

$\qty[per-mode = fraction]{1,345}{\coulomb\per\mole}$


there are more commonly used variants of quantity combinations:

$\qtylist{10;30;45}{\metre}$
$\qtyproduct{10 x 30 x 45}{\metre}$

Ranges works as well:

$\qtyrange{10}{30}{\metre}$
$\numrange{1}{10}$

AND if we need to reference some equations:

$$E = m \cdot c^{2}$$ {#eq:Einstein}

(works only with \$\$ ...)
and this gives us informations about the warp here @eq:Einstein and more whereever.
