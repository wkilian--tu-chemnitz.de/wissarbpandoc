

#### multicols 

(so it is "plain" tex but I think it can be included in MD easy)

\begin{multicols}{2}
\begin{enumerate}
\item 
Skiendenbereich/(+EDA)
\item 
Skischaufel/(+SPA)
\item
Skihinterteil
\item
Skivorterteil
\item
Hintere Kontaktlinie
\item
Vordere Kontaktlinie
\item
(+AL)
\item
Mitte Auflagelänge
\end{enumerate}
\end{multicols}

change this to this idea:

https://github.com/dialoa/columns.git

and it looks like this

:::columns
- Skiendenbereich
- Skischaufel
- Skihinterteil
- Skivorterteil
- Hintere Kontaktlinie
- Vordere Kontaktlinie
- (+AL)
- Mitte Auflagelänge
:::

:::columns
Section 1.3.1 Please specify the number of subjects that took part to experimental tests. These subjects are healthy or with multiple sclerosis? If the subjects are healthy, it is important to underline in the discussion the limitations of the validity of the results in the context multiple sclerosis (that could be characterized by different motor pattern). How many trials performed each subject? Sample frequency (10 fps) of the radar should be reported in this section. In addition, also for the use of pressure distribution measuring plates more details should be added (e.g., sample frequency and some details about the data processing in section 1.3.2). It is not clear the full design of experiments. Is it composed by 6 different configurations? 2 sensor positions x 3 different heights? I suggest to better explain this point. Some details about the use of the GoPro should be added. What was its setup? Which is the role of this system in the work?
:::

But this doesn't work with tables (yet). The author seems to plan to change this.
For now, we solve this with minipages and the famous "hide from pandoc helper" like this:

```markdown
\Begin{figure}
\Begin{minipage}{0.5\textwidth}

| ABC  | DEF  | Q       |
| ---- | ---- | ------- |
| Test | Case | Table 1 |

Table: First Strike {#tbl:FirstStrike}

\End{minipage}
\Begin{minipage}{0.5\textwidth}

| ghi  | DEF  | Q       |
| ---- | ---- | ------- |
| Test | Case | Table 2 | 

Table: Second Strike {#tbl:SecondStrike}

\End{minipage}
\End{figure}
```
\Begin{figure}
\Begin{minipage}{0.5\textwidth}

| ABC  | DEF  | Q       |
| ---- | ---- | ------- |
| Test | Case | Table 1 |

Table: First Strike {#tbl:FirstStrike}

\End{minipage}
\Begin{minipage}{0.5\textwidth}

| ghi  | DEF  | Q       |
| ---- | ---- | ------- |
| Test | Case | Table 2 | 

Table: Second Strike {#tbl:SecondStrike}

\End{minipage}
\End{figure}

this is the original workaround to have tables in multicol environments:

```latex
\makeatletter
\let\oldlt\longtable
\let\endoldlt\endlongtable
\def\longtable{\@ifnextchar[\longtable@i \longtable@ii}
\def\longtable@i[#1]{\begin{figure}[H]
\onecolumn
\begin{minipage}{0.5\textwidth}
\oldlt[#1]
}
\def\longtable@ii{\begin{figure}[H]
\onecolumn
\begin{minipage}{0.5\textwidth}
\oldlt
}
\def\endlongtable{\endoldlt
\end{minipage}
\twocolumn
\end{figure}}
\makeatother
```

but if we just want to place the tables side by side we can use the workaround above.

And don't forget to reference to @tbl:FirstStrike and to the @tbl:SecondStrike as usual.






