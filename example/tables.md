As tables lag support by pandoc in the two column mode we introduced a workaround to have it available (look for details at the template) but the workaround handle the tables as figures and minipage ---- aaaaaaand this may lead to confusing positioning of the tables. Figure {H} is used to minimize this problem.

```
  Right     Left     Center     Default
-------     ------ ----------   -------
     12     12        12            12
    123     123       123          123
      1     1          1             1

Table:  Demonstration of simple table syntax. {#tbl:ExampleTable}
```

  Right     Left     Center     Default
-------     ------ ----------   -------
     12     12        12            12
    123     123       123          123
      1     1          1             1

Table:  Demonstration of simple table syntax. {#tbl:ExampleTable}

It is possible to reference to tables as usual @tbl:ExampleTable

```
| Table Name    | Filter  | Name     | UseCase              |
| ------------- | ------- | -------- | --------------------:|
| Statement     | is used | the name | for examples         |
| Other Example | yes     | noname   | for examples as well | 

Table:  Demonstration of more complex table syntax. {#tbl:ComplexExampleTable}
```

| Table Name    | Filter  | Name     | UseCase              |
| ------------- | ------- | -------- | --------------------:|
| Statement     | is used | the name | for examples         |
| Other Example | yes     | noname   | for examples as well | 

Table:  Demonstration of more complex table syntax. {#tbl:ComplexExampleTable}

**Keep one space line between the table itself and the keyword "Table"**



