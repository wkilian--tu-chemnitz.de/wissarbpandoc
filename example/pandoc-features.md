
Footnotes (don't use this if you can somehow avoid it!)

```
Here is a footnote reference,[^1] and another.[^longnote]

[^1]: Here is the footnote.

[^longnote]: Here's one with multiple blocks.
```

Here is a footnote reference,[^1] and another.[^longnote]

[^1]: Here is the footnote.

[^longnote]: Here's one with multiple blocks.

and Lists...

- List
	- Sub Point
	- Second sub Point
- Second Point
	- Sub Second ;)
