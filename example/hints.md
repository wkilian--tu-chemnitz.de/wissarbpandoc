### protect from line break
to protect a space from a line break just place a backslash right before the space. Looks like this Figure\\ 1.1 is ... So "Figure" and the "1.1" wil not be broken.

to be checked if neccessary

### Chapter Options

```
Note on LaTeX and chapters option

Because pandoc-crossref offloads all numbering to LaTeX if it can, chapters: true has no direct effect on LaTeX output. You have to specify Pandoc's --top-level-division=chapter option, which should hopefully configure LaTeX appropriately.

It's a good idea to specify --top-level-division=chapter for any output format actually, because pandoc-crossref can't signal pandoc you want to use chapters, and vice versa.
```
