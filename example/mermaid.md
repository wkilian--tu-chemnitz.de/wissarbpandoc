We can create diagrams right from inside our cool thesis or paper using "mermaid"

https://github.com/raghur/mermaid-filter

```{.mermaid width=800 theme=forest caption="Mermaid Test" #fig:SimplifydStates}

stateDiagram-v2

direction LR

[*] --> Idle

DeepSleep --> WakeUpMeasurement

BLEDataSend --> Idle

DataFlashStore --> Idle

Measuring --> BLEDataSend

Measuring --> DataFlashStore

BLEDataSend --> DataFlashStore

Idle --> Idle

Idle --> DeepSleep

Idle --> Measuring

Idle --> SystemTasks

SystemTasks --> Idle

WakeUpMeasurement --> Idle
```

A very cool mermaid online editor can be found here:

https://mermaid-js.github.io/mermaid-live-editor/


