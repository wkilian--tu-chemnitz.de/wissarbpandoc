In Pandoc you can use plain Tex commands to force some special things like this:

```latex
Force a new page:
\newpage
Change numbering scheme 
\pagenumbering{arabic}
```
### paragraphs, spaces and things

And if we need a break aka paragraph, we can just insert a line

between the text or if we want to force it we need just more than 2 spaces at the end of the line  (followed by a new empty line)

like this. But we can also insert a dedicated empty line with the tex command

```latex
bla bla bla\newline

```
and this gives us a pretty 

bla bla bla\newline

space between two paragraphs. 


