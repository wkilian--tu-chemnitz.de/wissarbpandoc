!include ../example/codestyle.md

The code doesn't look that nice (without any modification) because we do not have any highlighting - but we can change this in the template like in this example:

https://www.overleaf.com/learn/latex/Code_listing

we just import the codestyle the same way we include any other file:

```
!include codestyle.md
```

Of course, we can include some source code as well, example is shown in @lst:cppcode or in @lst:haskelcode

```cpp
!include ../example/sourcecode/code_example.cpp
```

Listing: CPP code included {#lst:cppcodeinc}

```cpp
!include`startLine=1, endLine=10` ../example/sourcecode/code_example.cpp
```

Listing: Some partial included cpp {#lst:cppcode}

```haskell
main :: IO ()
main = putStrLn "Hello World!"
```
Listing: Some Haskel Code {#lst:haskelcode}  

*If you get something like this:
Error producing PDF.
! LaTeX Error: Invalid UTF-8 byte sequence (\\expandafter\\lst@FillFixed@).  
it is because of some special characters you try to use but you have UTF-8 disabled (eg with the IEEE template)*

