---
pandocomatic:
   use-template: SciWork
#header-includes: |
#   - |
#   ```{=latex}
#   \usepackage{listings}
#   ```
---

!include ./thesis_header.yaml



# this is the main document

Hauptdokument

## to show how to include aditional files (without counting the section) {-}

!include ./thesis_example_abstract.md

\newpage
\pagenumbering{arabic}


## show some thesis features

Show the thesis features - don't do empty text after headlines...


### abreviations

Abkürzungen werden so geschrieben (+BAR) 

### citing

und Zitate mit Refernz wie immer [@tanzawa2013] einfach so. 

### math

ein wenig Mathematik $a_{2}^{5}x \text{ bla } \dot{\vec{y}}$ (inline) and as we use the siunix we can write
numbers $\num{123,4}$
or numrange $\numrange{1}{10}$
and of course units
$\si{\newton} = \si{\kilo\gram\metre\per\square\second}$  
$\SI{100}{\km}$  
but if we need to reference some equations:
$$E = m \cdot c^{2}$$ {#eq:Einstein}
(works only with \$\$ ...)
and this gives us informations about the warp here @eq:Einstein and more whereever.


### pictures

Dieser Text beschäftigt sich mit Abbildungen. Dazu zeigt [Abbildung @fig:BildReferenz] ein Beispiel mit eigener "Abbildungsbezeichnung".
Dieser Text beschäftigt sich mit Abbildungen. Dazu zeigt @fig:BildReferenz ein Beispiel - diesmal mit automatisch generierter Bezeichnung.

![Ein Bild mit einer langen Unterschrift und Referenz [@tanzawa2013]](wissarbpandoc/example/pictures/bild.jpg){#fig:BildReferenz short-caption="short caption, if we need it short" width="70%" height="40%"}

To use the "short caption" option, the "short-caption.lua" filter is necessary.


### tables


  Right     Left     Center     Default
-------     ------ ----------   -------
     12     12        12            12
    123     123       123          123
      1     1          1             1

Table:  Demonstration of simple table syntax. {#tbl:ExampleTable}


It is possible to reference to tables as usual @tbl:ExampleTable



### multicols 

(so it is "plain" tex but I think it can be includet in MD easy)

\begin{multicols}{2}
\begin{enumerate}
\item 
Skiendenbereich/(+EDA)
\item 
Skischaufel/(+SPA)
\item
Skihinterteil
\item
Skivorterteil
\item
Hintere Kontaktlinie
\item
Vordere Kontaktlinie
\item
(+AL)
\item
Mitte Auflagelänge
\end{enumerate}
\end{multicols}




### code listings 



The code doesn't look that nice because we do not have any highlighting - but we can change this in the template like in this example:

https://www.overleaf.com/learn/latex/Code_listing

(in the thesis_example.md) we have the definition here. We could also include it from a dedicated file

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}


Of course, we can include some source code as well, example is shown in @lst:cppcode or in @lst:haskelcode

```cpp
!include sourcecode/code_example.cpp
```

Listing: Listing caption {#lst:cppcode}


```haskell
main :: IO ()
main = putStrLn "Hello World!"
```

: Listing caption {#lst:haskelcode}




\newpage

# Second chapter

Simple tips and hints

## hints

### protect from line break
to protect a space from a line break just place a backslash right before the space. Looks like this Figure\\ 1.1 is ... So "Figure" and the "1.1" wil not be broken.

to be checked if neccessary
i

```
Note on LaTeX and chapters option

Because pandoc-crossref offloads all numbering to LaTeX if it can, chapters: true has no direct effect on LaTeX output. You have to specify Pandoc’s --top-level-division=chapter option, which should hopefully configure LaTeX appropriately.

It’s a good idea to specify --top-level-division=chapter for any output format actually, because pandoc-crossref can’t signal pandoc you want to use chapters, and vice versa.
```


Neue Seite, denn ab hier kommen die Referenzen.
\newpage




