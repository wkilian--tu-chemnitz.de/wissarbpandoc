
WE can have pictures and references to the picture if we want to. References are done as usual:

```
Something is shown in @fig:PicRef.
```

Something is shown in @fig:PicRef.

And if we want to add our own prefix we just write it like this:

```
Something is shown in [FancyPicture @fig:PicRef].
```

Something is shown in [FancyPicture @fig:PicRef].

```
![Picture with a very long caption and some reference [@hol2006] as this would be way to long for the list of pictures we can have a short caption as well](../example/pictures/bild.jpg){#fig:PicRef short-caption="short caption, if we need it short" width="40%" height="15%"}
```

![Picture with a very long caption and some reference [@hol2006] as this would be way to long for the list of pictures we can have a short caption as well](../example/pictures/bild.jpg){#fig:PicRef short-caption="short caption, if we need it short" width="40%" height="23%"}

We shrink this picture in this example to fit inside a two column paper.

We can also have capitalized reference prefixes:

```
Something is shown in @Fig:PicRef
```

Something is shown in [@Fig:PicRef]

Or we can have subfigures eg. side by side


```
<div id="fig:subfigures">
![Subfigure a](../example/pictures/bild.jpg){width=30% height=30%}
![Subfigure b](../example/pictures/bild.jpg){#fig:subfigureB width=30% height=30%}

Subfigures caption
</div>
```


<div id="fig:subfigures">
![Subfigure a](../example/pictures/bild.jpg){width=30% height=30%}
![Subfigure b](../example/pictures/bild.jpg){#fig:subfigureB width=30% height=30%}

Subfigures caption
</div>

Subfigures with references are supported, see [@fig:subfigures; @fig:subfigureB]

```
Subfigures are supported, see [@fig:subfigures; @fig:subfigureB]
```





Link to the Pandoc Crossref -
https://lierdakil.github.io/pandoc-crossref/#subfigures

**Important note: everything to the picture is in the same line and without spaces!**

To use the "short caption" option, the "short-caption.lua" filter is necessary.
