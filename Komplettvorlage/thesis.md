---
style: 'Masterarbeit'
title: 'Das hat noch gar keinen Namen'
subtitle: '(optional) Hier ist die nähere Erläuterung'
author:  Wolfgang Kilian
date: 27-05-2019
lang: de-DE
bibliography: Literatur/Literatur.bib
#Nachfolgende Änderungen sind optional
#reference-section-title: Literaturverzeichnis
#toc-own-page: true
#numbersections: true
#titlepage: true
#caption-justification: centering
#fontsize: 11pt
#figureTitle: "Abbildung"
#tableTitle: "Tabelle"
#secTitle: "Absatz"
#figPrefix: "Abb."
#eqnPrefix: "Gl."
#tblPrefix: "Tbl."
#secPrefix: "Abs."
#loftitle: "# Abbildungsverzeichnis"
#lotTitle: "# Tabellenverzeichnis"
#titlepage: true
#toc: true
#toc-own-page: true
#numbersections: true
#mainfont: Font-Regular.otf
#mainfontoptions:
#  - BoldFont=Font-Bold.otf
#  - ItalicFont=Font-Italic.otf
#  - BoldItalicFont=Font-BoldItalic.otf
#fontfamily: merriweather
#fontfamilyoptions: sfdefault
#mainfont: 'Palatino'
# Sektionen - ob das so klappt?
abstract: 'include/abstract'
introduction: 'include/introduction'
chapters:
    - 'include/bla1'
    - 'include/blubber'
    - 'include/next'
    - 'include/overview'
conclusion: 'include/conclusion'
Watermark: 'Entwurf' # Wenn das gesetzt ist, dann kommt wird es erzeugt, wenn nicht, ignoriert
mainfont: XITS
# Das muss auch alles noch in die eigentliche Vorlage, dass Template integriert werden
header-includes:
      - \usepackage{unicode-math}
      - \setmathfont{XITS Math}
      - \setmathfont[range={\mathcal,\mathbfcal},StylisticSet=1]{XITS Math}
      - \usepackage{multicol}
      - \usepackage{pstricks}
      - \usepackage{pst-barcode}
      - \usepackage{draftwatermark}
      - \SetWatermarkText{Entwurf}
      - \SetWatermarkLightness{0.95}
      - \newcommand{\hideFromPandoc}[1]{#1}\hideFromPandoc{\let\Begin\begin\let\End\end}
commandline:
  csl: "`citestyle.csl`"
---


Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
no sea takimata sanctus est Lorem ipsum dolor sit amet.


!include chapters/chap01.md




