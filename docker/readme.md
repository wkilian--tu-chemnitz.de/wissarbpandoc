

# Docker bauen (zB)

sudo docker build -t pantest .


# Docker verwenden um thesis_example im SciWork template zu übersetzen

sudo docker run -i -t --volume "`pwd`:/data" --user `id -u`:`id -g` pantest --debug --config WissArb.yaml -i thesis_example.md



