# This Docker is intendet to be used for our pandoc to tex workflow
# it will use the arch linux system because im familiar with tis
# problem - rolling release (!) so we have to fight with specific versions
# for all packages, but eventualy it will work as well

FROM library/archlinux:base-devel-20220116.0.44468

# as we do build some packages from aur we use a dedicadet build user

RUN useradd --no-create-home --shell=/bin/false build && usermod -L build
RUN echo "build ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN echo "root ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN chmod 777 /tmp
# we create a specific pandoc user that holds all the filters, templates aso
RUN useradd --shell=/bin/false pandoc && usermod -L pandoc
RUN mkdir --parent /home/pandoc/pandoc/

# if we fix the release to a specific date we will get into trouble with sign checks, so first we get all the keys
# and fix the release in the next steps
# this doesnt work, because the first thing pacman is doing is to install the old keyfile from the snapshot

RUN pacman-key --init
## just to make sure, everything is there
RUN pacman-key --populate

##### fix pacman to a specific date ##### (like a snapshot for a rolling release)

RUN echo 'Server=https://archive.archlinux.org/repos/2022/01/24/$repo/os/$arch' > /etc/pacman.d/mirrorlist

# THIS IS DANGEROUS ! We ignore the package signing from this point on! If someone have access to the archive, he can manipulate the system.
# Is there a better way to handle this at this point? Even with libfaketime it is not possible, because we have to install it before we can use it
# maybe we can exclude the keyring from the update?

RUN sed -i 's/SigLevel[[:space:]]*=[[:space:]]Required/SigLevel = Never/g' /etc/pacman.conf

## update the whole system (to the specified base level above)

RUN pacman -Syyuu --noconfirm --ignore archlinux-keyring
RUN pacman -S git --noconfirm

# since we deactivated the integrity check we can skip this



#RUN pacman-key --init
## just to make sure, everything is there
#RUN pacman-key --populate



##### some pandoc dependencies - seems that they are not installed well ... #####
##### pandoc itself ######

RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/p/pandoc/pandoc-2.14.2-14-x86_64.pkg.tar.zst

##### LaTex #####

USER root
run pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/t/texlive-core/texlive-core-2021.61403-1-any.pkg.tar.zst

# other latex packets we need
user root
RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/t/texlive-science/texlive-science-2021.61383-1-any.pkg.tar.zst
RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/t/texlive-latexextra/texlive-latexextra-2021.61405-1-any.pkg.tar.zst

##### Everything around the TeX #####

# enumitem 3.9

WORKDIR "/tmp"
USER build
RUN git clone https://aur.archlinux.org/latex-enumitem.git 
WORKDIR "/tmp/latex-enumitem"
RUN git checkout f73f22aa9768d4341697d1cf3db4f3405c674ace
RUN makepkg -si --noconfirm --asdeps

# xits 1.302

WORKDIR "/tmp"
USER build
RUN git clone https://aur.archlinux.org/otf-xits.git
WORKDIR "/tmp/otf-xits" 
RUN git checkout fa9940a979f9c82dd8120426a96e0eb8e427aa9c
RUN makepkg -si --noconfirm --asdeps

##### pandoc filters #####

# Install pypthon-panflute 2.1.3-1

WORKDIR "/tmp"
USER build
RUN git clone https://aur.archlinux.org/python-panflute.git
WORKDIR "/tmp/python-panflute"
RUN git checkout 30120c19991accc9f0f4d87deeaf985dfecd0e6f
RUN makepkg -si --noconfirm --asdeps


# Install Pandoc include -- 0.8.7-2
# dependencys

WORKDIR "/tmp"
USER root
RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/p/python-setuptools/python-setuptools-1:59.1.1-1-any.pkg.tar.zst

USER build
RUN git clone https://aur.archlinux.org/python-pandoc-include.git
WORKDIR "/tmp/python-pandoc-include"
RUN git checkout b684941373373ad24e0ee88f2423065c5eef69c5
RUN makepkg -si --noconfirm --asdeps

# Install Mermaid-filter

# Mermaid filter dependency 
# Chromium
WORKDIR "/tmp"
USER root
RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/c/chromium/chromium-97.0.4692.99-3-x86_64.pkg.tar.zst

# Mermaid filter itself
WORKDIR "/tmp"
USER root
RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/n/npm/npm-8.3.2-1-any.pkg.tar.zst
RUN npm install --global mermaid-filter

# Mermaid itself
USER build
RUN git clone https://aur.archlinux.org/mermaid-cli.git
WORKDIR "/tmp/mermaid-cli"
RUN git checkout 990e6db

# npm hardly wants a home dir if build with user "build"
USER root
RUN mkdir /home/build
RUN chown build:build -R /home/build

USER build
RUN makepkg
USER root
RUN pacman --asdeps --noconfirm -U mermaid-cli-9.0.3-1-any.pkg.tar.zst

# should be includet in the dependancy installed before
#WORKDIR "/tmp"
#USER root
#RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/a/atk/atk-2.36.0-1-x86_64.pkg.tar.zst


# Install Pandoc crossref 0.3.12.0-118

WORKDIR "/tmp"
USER root
RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/p/pandoc-crossref/pandoc-crossref-0.3.12.0-118-x86_64.pkg.tar.zst

##### create data directory #####
USER root
RUN mkdir --parents /root/.pandoc/filters

##### LUA filter #####
# Install pandoc-gls

WORKDIR "/tmp"
USER build
RUN git clone https://github.com/tomncooper/pandoc-gls.git
WORKDIR "/tmp/pandoc-gls"
RUN git checkout 89d3e716d3778af5d92ce735c9beb1d53826bbb1
USER root
RUN cp pandoc-gls.lua /home/pandoc/pandoc/pandoc-gls.lua

# Install pandoc-short-captions
# take it from the pandoc-lua-filters repository

WORKDIR "/tmp"
USER build
RUN git clone https://github.com/pandoc/lua-filters
WORKDIR "/tmp/lua-filters"
RUN git checkout ffcd913542f96ceb7289e49e3c70dd644106e4d2
USER root
RUN cp short-captions/short-captions.lua /home/pandoc/pandoc/short-captions.lua

# Install columns
# take it from the pandoc-lua-filters repository

WORKDIR "/tmp"
USER build
RUN git clone https://github.com/dialoa/columns.git
WORKDIR "/tmp/columns"
RUN git checkout 3d0238c07fd4132a4e5d41ec1de19beb2c61eb4e
USER root
RUN cp columns.lua /home/pandoc/pandoc/columns.lua



##### Pandoc Helpers #####
# install pandocomatic
# ok it goes to /usr/sbin - not where it should be but on the other hand -
# we are inside a docker...


WORKDIR "/tmp"
user root
RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/r/rubygems/rubygems-3.2.29-1-any.pkg.tar.zst


USER build
#RUN git clone https://github.com/htdebeer/pandocomatic.git
#WORKDIR "/tmp/pandocomatic"
#RUN git checkout 7c2beab98c8bbd9e2a5039fdbb31fde93ad4703e
USER root
# some dependencys are installed "by hand" to avoid further version trouble
RUN gem install --no-user-install paru --version 0.4.3
RUN gem install --no-user-install optimist --version 3.0.1
# and now pandocomati itself
RUN gem install --no-user-install pandocomatic --version 0.2.8

##### The Templates and everything else we need #####
# so we just clone ourselve

WORKDIR "/tmp"
#USER root
USER build
ARG build=7
RUN git clone https://gitlab.hrz.tu-chemnitz.de/wkilian--tu-chemnitz.de/wissarbpandoc.git
WORKDIR "/tmp/wissarbpandoc"
USER root
#RUN cp thesis.tex /usr/share/pandoc/data/templates/SciWork.latex
RUN cp example_thesis/SciWork.latex /usr/share/pandoc/data/templates/SciWork.latex
RUN cp example_paper/TIeee.tex /usr/share/pandoc/data/templates/TIeee.latex
RUN cp example_paper/TSpringer.tex /usr/share/pandoc/data/templates/TSpringer.latex
RUN cp example_paper/TVde.tex /usr/share/pandoc/data/templates/TVde.latex
RUN cp example_paper/TSpringer_Chapters.tex /usr/share/pandoc/data/templates/TSpringer_Chapters.latex


##### temp to be moved wherever it belongs #####



#move me to the lua filter section
# Install longtable-to-xtab 
# take it from the dialoa/dialectica-filters repository

WORKDIR "/tmp"
USER build
RUN git clone https://github.com/dialoa/dialectica-filters.git
WORKDIR "/tmp/dialectica-filters"
RUN git checkout cd65fc823d78dd32f95adc83672e538c39d1eca3
USER root
RUN cp longtable-to-xtab/longtable-to-xtab.lua /home/pandoc/pandoc/longtable-to-xtab.lua




WORKDIR "/tmp"
user root
#RUN pacman --asdeps --noconfirm -U https://archive.archlinux.org/packages/h/haskell-zip-archive/haskell-zip-archive-0.4.1-155-x86_64.pkg.tar.zst




######################
# get docker to work #
######################

USER root

ENV XDG_DATA_HOME=/home/pandoc

###### put this where it belongs to ######

RUN chown pandoc:pandoc -R /home/pandoc
RUN chmod 777 -R /home/pandoc

WORKDIR /data

ENTRYPOINT ["/usr/sbin/pandocomatic"]
USER pandoc


