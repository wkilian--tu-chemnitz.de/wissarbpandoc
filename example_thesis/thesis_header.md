---
title: 'Das hat noch gar keinen Namen'
subtitle: '(optional) Hier ist die nähere Erläuterung'
author:  'Wolfgang Kilian'
date: '27-05-2019'
place: 'Chemnitz'
title-meta: 'Der Titel vom PDF'
author-meta: 'Der Author vom PDF'
subject: 'Subject vom PDF'
keywords: 'Thesis, New'
Supervision: yes
Pruefer: 'the choosen one'
Supervisor: 'Supervisor I'
SecSupervisor: 'Supervisor II'
#Company: 'The providing company'
#SecCompany: 'The company extension'
defaultlanguage: german
defaultlanguageoptions: spelling=new
otherlanguage: english
otherlanguageoption: variant=british
hidelinks: yes
siunitx: yes
si-locale: DE
mainfont: XITS
math-mainfont: XITS
toc: yes
toc-title: 'Table of content'
toc-own-page: yes
toc-depth: 4
numbersections: 5
first-chapter: 1
startroman: yes
lof: yes
lot: yes
Titelpage: yes
Logo: 'Logo.png'
glossary: '../example/myglossary'
glossary-title: 'Glossar'
bibliography: '../example/literature/literature.bib'
csl-refs: yes
csl-hanging-indent: yes
csl: 'apa.csl'
multicols: false # automatically selected with the columns lua Filter
watermark: 'Entwurf'
codeBlockCaptions: true
listings: true
floathelper: true
rotatesupport: true
hyperref: true
fixHyperrefSvmult: false
header-includes:
  - \newcommand{\hideFromPandoc}[1]{#1}
  - \hideFromPandoc{
      \let\Begin\begin
      \let\End\end
    }
---
