---
pandocomatic:
   use-template: SciWork
---

!include ./thesis_header.md

!include ../example/codestyle.md


# this is the main document

We can include any files inside our main document. This is done with the pandoc-include filter:
https://pypi.org/project/pandoc-include/

# How to chapter and section

!include ../example/sections.md

# Abstract

!include ../example/abstract.md

# Features to use

The following chapters shows how to use the markdown-pandoc-latex + (some magic) combination.

## Some tex inside Markdown

!include ../example/tex_inside.md

\newpage
\pagenumbering{arabic}


## show some pandoc features

Show the thesis features - don't do empty text after headlines...

!include ../example/pandoc-features.md

### Abreviations

!include ../example/abreviations.md

### citing

!include ../example/citing.md


### math

!include ../example/math.md

### pictures

!include ../example/pictures.md

### Tables
!include ../example/tables.md

### Fancy things
!include ../example/fancy.md

### Code listings 
!include ../example/sourcecode.md

# Next chapter
Simple tips and hints

## hints

!include ../example/hints.md

# References



